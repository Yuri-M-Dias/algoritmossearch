/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmossearch;

/**
 *
 * @author Yuri
 */
public class AlgoritmosSearch {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        BinaryTree binary = new BinaryTree();
        int max = 1000;
        for(int j = 0; j < 100; j++){
            binary.insert(0 + (int)(Math.random() * ((max - 0) + 1)));
        }
        binary.printTree();
        binary.printPaths();
        binary.printPostorder();
        System.out.println(binary.lookup(2));
        System.out.println(binary.maxDepth());
        System.out.println(binary.minValue());
    }
}
